interface Meta {
    code: number;
    message: string;
}

interface Provider {
    id: number;
    name: string;
}

interface Category {
    id: number;
    name: string;
}

interface Topic {
    id: number;
    name: string;
}

interface Level {
    id: number;
    name: string;
}

interface DateCourse {
    name: string;
    value?: any;
}

interface Rating {
    value: number;
    count: number;
}

interface PriceName {
    id: number;
    name: string;
}

export interface ICourses {
    created_at: string;
    id: number;
    name: string;
    coach: string;
    duration: number;
    provider: Provider;
    category: Category;
    topic: Topic;
    level: Level;
    language: any[];
    date_course: DateCourse;
    rating: Rating;
    description: string;
    price_name: PriceName;
    price: number;
    freemium_code?: any;
    photo: string;
    liked: boolean;
}

export interface RootCourses {
    meta: Meta;
    data: ICourses[];
}


export const getCourses = async () =>{
    const { $http } = useNuxtApp();
    const { pending: pendingCourse, data: courses } = await useLazyAsyncData<RootCourses>('course', async () => await $http.get('v1/courses'))
    console.log("pendingCourses", pendingCourse)
    return {pendingCourse, courses}
}
export const getTopics = async () =>{
    const { $http } = useNuxtApp();
    const { pending: pendingTopics, data: topics } = await useLazyAsyncData<RootCourses>('topics', async () => await $http.get('v1/topics'))
 
    return {pendingTopics, topics}
}
export const getCategories = async () =>{
    const { $http } = useNuxtApp();
    const { pending: pendingCategories, data: categories } = await useLazyAsyncData<RootCourses>('categories', async () => await $http.get('v1/categories'))
    return {pendingCategories, categories}
}
export const getLevels = async () =>{
    const { $http } = useNuxtApp();
    const { pending: pendingLevels, data: levels } = await useLazyAsyncData<RootCourses>('level', async () => await $http.get('v1/levels'))
    return {pendingLevels, levels}
}
