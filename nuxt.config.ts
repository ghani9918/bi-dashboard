const apiUrl = process.env.API_URL ?? 'https://bi-api.bayunovianto.com/api/';

export default defineNuxtConfig({
    plugins: [
        { src: '~/plugins/charts.ts', mode: 'client' },
    ],
    modules: [
        'nuxt-windicss',
    ],
    ssr: false,
    runtimeConfig: {
        public: {
            apiUrl,
        }
    },
})
