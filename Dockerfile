FROM node:18.12.1-alpine3.16

WORKDIR /app
COPY . .

RUN npm i && npm run build

ENV NITRO_HOST=0.0.0.0
ENV NITRO_PORT=8080

EXPOSE 8080

ENTRYPOINT ["node", ".output/server/index.mjs"]
