import { defineNuxtPlugin, useRuntimeConfig } from '#app';

export default defineNuxtPlugin(async (nuxtApp) => {
  const { public: config } = useRuntimeConfig();

  const defaultOptions: any = {
    baseURL: config.apiUrl,
    async onResponse({ request, response, options }) {
      console.log('[fetch response]')
    },
    async onResponseError({ request, response, options }) {
      console.log('[fetch response error]');
      console.log(response);
    },
    async onRequest({ request, options }) {
        console.log(request)
      console.log('[fetch request]')
    },
    async onRequestError({ request, options, error }) {
      console.log('[fetch request error]')
      console.error(error);
    },
  }

  const http = {
    get: <T>(url: string, options: any = {}) => {
      return $fetch<T>(url, { ...defaultOptions, ...options, method: 'GET'});
    },
    post: <T>(url: string, body: any, options: any = {}) => {
      return $fetch<T>(url, {...defaultOptions, ...options, method: 'POST', body});
    },
    put: <T>(url: string, body: any, options: any = {}) => {
      return $fetch<T>(url, {...defaultOptions, ...options, method: 'PUT', body});
    },
    delete: <T>(url: string, options: any = {}) => {
      return $fetch<T>(url, {...defaultOptions, ...options, method: 'DELETE'});
    },
  };


  return {
    provide: {
      http,
    },
  }
})
